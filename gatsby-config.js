require("dotenv").config({
  path: `.env`,
});

module.exports = {
  plugins: [  {
    resolve: `gatsby-plugin-intl`,
    options: {
      // language JSON resource path
      path: `${__dirname}/src/intl`,
      // supported language
      languages: [`en`, `pl`, `de`],
      // language file path
      defaultLanguage: `en`,
      // option to redirect to `/ko` when connecting `/`
      redirect: true,
    },
  },
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sass",
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-source-strapi",
      options: {
        apiURL: process.env.API_URL || "http://pressly-backend.pressly-web.pl",
        collectionTypes: ["article", "category", "writer",],
        singleTypes: [`homepage`, `global`, `gamification`, `realizations`, `soft`, `offer`, `collab`, `website`, `shops`, `outsourcing`, `ruby`, `php`, `react`, `career`,`java-script`,],
        queryLimit :20000,
      },
    },

    
    "gatsby-plugin-image",
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "gatsby-starter-default",
        short_name: "starter",
        start_url: "/",
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`,
  
      },
    },
    "gatsby-plugin-offline",
  ],
};
