import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";
import {NavItem, NavLink} from "reactstrap";
import "../assets/css/navbar.scss";

const Nav = () => {
  const navItems = [
    {
        path: "/offer",
        label: "Oferta"
    },
    {
        path: '/collab',
        label: "Współpraca"
    },
    {
        path: "/realizations",
        label: "Realizacje"
    },
    {
        path: "/gamification",
        label: "Grywalizacja"
    },
    {
      path: "/career",
      label: "Kariera"
    },
    {
        path: "/blog",
        label: "Blog"
    },
    
    
]

    
    return <> 
      <div>
          <nav className="navbar__wrapper">
      
            <div className="navbar__wrapper-content">
              <div className="navbar__left">
              <Link to="/#" className="navbar__logo">Pressly.</Link>
            </div>
              
              
              <div className="navbar__content">
                <ul className="navbar__ul">
                {navItems.map(item => <NavItem><NavLink href={item.path}>{item.label}</NavLink></NavItem>)}
                </ul>
                
              </div>
              <Link to="/contact">
              <button
                className="navbar__button"
                type="button"
              >
                KONTAKT
              </button>
              </Link>
            </div>
          </nav>
        </div>

    </>
};

export default Nav;
