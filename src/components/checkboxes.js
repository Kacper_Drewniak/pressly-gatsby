import React from 'react';
import ReactDOM from 'react-dom';
import { Formik, Field, Form } from 'formik';
import "../assets/css/checkboxes.scss";

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const Checkboxes = () => (
    <div>
        <h1 className="checkboxes__title">Lorem ipsum</h1>
        <h2 className="checkboxes__subtitle">1. Wybierz technologię</h2>
        <Formik
            initialValues={{
            toggle: false,
            checked: [],
            }}
            onSubmit={async (values) => {
                await sleep(500);
                alert(JSON.stringify(values, null, 2));
            }}
        >
            {({ values }) => (
            <Form>
                {/* 
                This first checkbox will result in a boolean value being stored. Note that the `value` prop
                on the <Field/> is omitted
            */}
                {/* <label>
                <Field type="checkbox" name="toggle" />
                {`${values.toggle}`}
                </label> */}

                {/* 
                Multiple checkboxes with the same name attribute, but different
                value attributes will be considered a "checkbox group". Formik will automagically
                bind the checked values to a single array for your benefit. All the add and remove
                logic will be taken care of for you.
            */}
            <div role="group" aria-labelledby="checkbox-group" className="checkboxes__wrapper">
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="React.js" />
                    <div className="checkbox"></div>
                    React.js
                </label>
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="React Native" />
                    <div className="checkbox"></div>
                    React Native
                </label>
                <label className="checkbox__single">
                    <Field className="check" type="checkbox" name="checked" value="Ruby on Rails" />
                    <div className="checkbox"></div>
                    Ruby on Rails
                </label>
                <label className="checkbox__single">
                    <Field  type="checkbox" name="checked" value="Node.js" />
                    <div className="checkbox"></div>
                    Node.js
                </label>
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="JavaScript" />
                    <div className="checkbox"></div>
                    JavaScript
                </label>
                <label className="checkbox__single" >
                    <Field type="checkbox" name="checked" value="PHP" />
                    <div className="checkbox"></div>
                    PHP
                </label>
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="UI/UX" />
                    <div className="checkbox"></div>
                    UI/UX
                </label>
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="WordPress" />
                    <div className="checkbox"></div>
                    WordPress
                </label>
                <label className="checkbox__single">
                    <Field type="checkbox" name="checked" value="PrestaShop" />
                    <div className="checkbox"></div>
                    PrestaShop
                </label>
            </div>

            <h2 className="checkboxes__subtitle2">2. Wybierz poziom</h2>

            <div role="group" aria-labelledby="checkbox-group" className="checkboxes__wrapper2">
                <label className="checkbox__single2">
                    <Field type="checkbox" name="checked" value="Junior" />
                    <div className="checkbox2"></div>
                    Junior
                </label>
                <label className="checkbox__single2">
                    <Field type="checkbox" name="checked" value="Medium" />
                    <div className="checkbox2"></div>
                    Medium
                </label>
                <label className="checkbox__single2">
                    <Field type="checkbox" name="checked" value="Senior" />
                    <div className="checkbox2"></div>
                    Senior
                </label>
                <label className="checkbox__single2">
                    <Field type="checkbox" name="checked" value="All" />
                    <div className="checkbox2"></div>
                    Wszystkie
                </label>
            </div>

            <button type="submit">Submit</button>
        </Form>
        )}
        
    </Formik>
    </div>
);

export default Checkboxes;