import React from "react";
import PropTypes from "prop-types";
import { StaticQuery, graphql } from "gatsby";
import Nav from "./nav";
import Footer from "./footer";

const Layout = ({ children}) => (
  <StaticQuery
    query={graphql`
      query {
        strapiHomepage {
          Title
          description
        } 
      }
    `}
    render={(data) => (
      <>
        <Nav />
        <main>{children}</main>
        <Footer />
      </>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
