import React, { useState } from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/collab.scss";
import Footer from "../components/footer";

const collab = () => {

    // eslint-disable-next-line
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const collabSingle = data.allStrapiCollab.nodes[0].collab_single;
    const faq = data.allStrapiCollab.nodes[0].faq;
    // eslint-disable-next-line
    const [isActive, setActive] = useState("false");
    const Toggle = () => {
    setActive(!isActive); 
    };

    return <>
    <Nav/>
    <div className="collab__section1">
        <div className="collab__left">
            <img src={`${strapiHost}${data.strapiCollab.img.url}`} className="collab__img"/>
        </div>
        <div className="collab__right">
            <h1 className="collab__right-title">{data.strapiCollab.title}</h1>
            <h2 className="collab__right-subtitle">{data.strapiCollab.subtitle}</h2>
            <p className="collab__right-description">{data.strapiCollab.description}</p>
        </div>
    </div>
    <h2 className="collab__title2">{data.strapiCollab.title2}</h2>

    <div className="collab__wrapper">
    {collabSingle.map(({description, step, title}) => 
            <div className="collab__single">
                <h2 className="collab__single-title">{title}</h2>
                <p className="collab__single-desc1">{description}</p>
                <p className="collab__single-step">{step}</p>
            </div>
    )}
    </div>
            <button
                className="collab__button"
                type="button"
                >
                <Link to="/contact">
                Zapytaj o bezpłatną wycenę
                </Link>
            </button>

            <h2 className="collab__faq-title">{data.strapiCollab.title_faq}</h2>

            <div className="collab__faq">
                
                <div className="collab__faq-left">
                    <img src={`${strapiHost}${data.strapiCollab.img_faq.url}`} className="collab__faq-img"/>
                </div>
                <div className="collab__faq-right">
                    {faq.map(({answear, question}, index) => 
                    <div key={index} className="collab__faq-single">
                        <div className="collab__faq-question-wrapper">
                        <p className="collab__faq-question" onClick={Toggle}>{question}</p>
                        </div>
                        <p className={isActive ? "collab__faq-answear" : "collab__faq-answear-active"}>{answear}</p>
                    </div>
                    )}
                </div>
            </div>

    <Footer/>
    </>
};

const query = graphql`
    query {
        strapiCollab {
            title
            img {
                url
                }
            subtitle
            title2
            description
            title_faq
            img_faq {
                url
            }
        }
            allStrapiCollab {
                nodes {
                    collab_single {
                    step
                    title
                    description
                    }
                    faq {
                        question
                        answear
                    }
                }
            }
    }`;

export default collab;