/* eslint-disable */
import React,{useEffect,useState} from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import Footer from "../components/footer";
import "../assets/css/career.scss";
import { useIntl } from "gatsby-plugin-intl"
import request from "graphql-request";

const career = () => {

  const intl = useIntl()

  useEffect(() => {
    const fetchProducts = async () => {
      const res = await request(
        'http://pressly-backend.pressly-web.pl/graphql', `
           {
            career(locale: "${intl.locale}") {
              id
              title
              description
            }
          } `
      );
      console.log(res)
    };
    fetchProducts();
  }, []);

    console.log(intl.locale)
    return <>
    <Nav/>

{/*    <div className="career__wrapper">*/}
{/*        <div className="career__section1">*/}
{/*            <div className="career__left">*/}
{/*                <img src={`${strapiHost}${data.strapiCareer.img.url}`} className="career__img"/>*/}
{/*            </div>*/}
{/*            <div className="career__right">*/}
{/*                <h1 className="career__right-title">{data.strapiCareer.title}</h1>*/}
{/*                <p className="career__right-description">{data.strapiCareer.description}</p>*/}
{/*            </div>*/}
{/*            */}
{/*        </div>*/}
{/*        <h2 className="career__title2">{data.strapiCareer.title2}</h2>*/}
{/*        <div className="career__wrapper-offer">*/}
{/*            {careerSingle.map(({description, title}) => */}
{/*            <div className="career__single-offer">*/}
{/*                <h2 className="career__single-title">{title}</h2>*/}
{/*                <p className="career__single-desc1">{description}</p>*/}
{/*            </div>*/}
{/*    )}*/}
{/*    </div>*/}
{/*</div>*/}

    <Footer/>
    </>

};

const query = graphql`
     {
strapiCareer {
    title
    description
    img {
        url
    }
    title2
    }
    allStrapiCareer {
        nodes {
            projects {
            description
            id
            title
            }
        }
        }
}`;

export default career