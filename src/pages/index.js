import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { GatsbyImage } from "gatsby-plugin-image";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/homepage.scss";
import "../assets/css/main.scss";
import ReactTypingEffect from 'react-typing-effect';
import Footer from "../components/footer";
import Rocket from "../images/Rocket3.svg";
import Background from "../images/background-rocket.svg";
import CountUp from 'react-countup';
// import Clutch from "../components/clutch";

const IndexPage = () => {
const data = useStaticQuery(query);
const strapiHost = process.env.GATSBY_API_URL;
return <>
    
        <Nav/>
        <div className="home__wrapper">
            <div className="home__section1">
                <div className="home__left1">
                    <h1 className="home__title1">{data.strapiHomepage.Title}</h1>
                    {/* <h2 className="home__title2">{data.strapiHomepage.title2}</h2> */}
                    <ReactTypingEffect className="home__title2"
                    text={["Bardzo skutecznie."]}
                    />

                    <p className="home__description">{data.strapiHomepage.description}</p>
                </div>
                <div className="home__right1">
                    <div className="home__img1-wrapper">
                        
                        <img className="rocket__svg" src={Rocket}/>
                        <img className="background__svg" src={Background}/>
                        {/* <img src="http://localhost:4000/uploads/we_love_pizza_2d1283bfa1.jpg"/> */}
                    </div>
                </div>
            </div>
            <div className="home__section3">
                <h2 className="home__section3-title">{data.strapiHomepage.title3}</h2>
                <div className="home__section3-content">

                
                    <Link to="/outsourcing" className="home__section3-rectangle1">
                        
                            <img src={`${strapiHost}${data.strapiHomepage.icon1.url}`}/>
                            <h2 className="home__section3-desc">{data.strapiHomepage.description2}</h2>
                        
                    </Link>
                    

                    
                    <Link to="/shops" className="home__section3-rectangle2">
                        
                            <img src={`${strapiHost}${data.strapiHomepage.icon2.url}`}/>
                            <h2 className="home__section3-desc1">{data.strapiHomepage.description3}</h2>
                        
                    </Link>
                    

                    
                        <Link to="/website" className="home__section3-rectangle3">
                            
                            <img src={`${strapiHost}${data.strapiHomepage.icon3.url}`}/>
                            <h2 className="home__section3-desc2">{data.strapiHomepage.description4}</h2>
                        </Link>
                    

                    
                        <Link to="/soft" className="home__section3-rectangle4">
                            <img src={`${strapiHost}${data.strapiHomepage.icon4.url}`}/>
                            <h2 className="home__section3-desc3">{data.strapiHomepage.description5}</h2>
                        </Link>
                </div>
            </div>

            <div className="home__section4-clutch">
                {/* <div className="home__section4-content"> */}
                    <div className="home__section4-left">
                        {/* <Clutch/> */}
                    </div>

                    <div className="home__section4-right">
                        <h2 className="home__section4-title">{data.strapiHomepage.title5}</h2>
                        <p className="home__section4-desc">{data.strapiHomepage.description_clutch}</p>
                    </div>
                {/* </div> */}
            </div>

            <div className="home__section5-technologies">
                <div className="home__section5-content">
                    <div className="home__section5-rectangle">
                        <img src={`${strapiHost}${data.strapiHomepage.technology1.url}`}/>
                        <p className="home__section5-desc1">{data.strapiHomepage.description_technology}</p>
                    </div>
                    <div className="home__section5-rectangle2">
                        <img src={`${strapiHost}${data.strapiHomepage.technology2.url}`}/>
                        <p className="home__section5-desc1">{data.strapiHomepage.description2_technology}</p>
                    </div>
                    <div className="home__section5-rectangle3">
                        <img src={`${strapiHost}${data.strapiHomepage.technology3.url}`}/>
                        <p className="home__section5-desc1">{data.strapiHomepage.description3_technology}</p>
                    </div>
                    <div className="home__section5-rectangle4">
                        <img src={`${strapiHost}${data.strapiHomepage.technology4.url}`}/>
                        <p className="home__section5-desc1">{data.strapiHomepage.description4_technology}</p>
                    </div>
                </div>
            </div>

            <div className="home__section6-wrapper">

                <h2 className="home__section6-title">{data.strapiHomepage.counters_title}</h2>

                <div className="home__section6-counters">
                    <div className="home__section6-single1">
                        <div className="home__section6-circle1">
                            <p className="home__section6-number1"><CountUp end={100} /></p>
                        </div>
                        <p className="home__section6-desc">{data.strapiHomepage.counter1_desc}</p>
                    </div>

                    <div className="home__section6-single2">
                        <div className="home__section6-circle2">
                            <p className="home__section6-number2"><CountUp end={100} /></p>
                        </div>
                        <p className="home__section6-desc">{data.strapiHomepage.counter2_desc}</p>
                    </div>

                    <div className="home__section6-single3">
                        <div className="home__section6-circle3">
                            <p className="home__section6-number3"><CountUp end={100} /></p>
                        </div>
                        <p className="home__section6-desc">{data.strapiHomepage.counter3_desc}</p>
                    </div>

                    <div className="home__section6-single4">
                        <div className="home__section6-circle4">
                            <p className="home__section6-number4"><CountUp start={30}
                            end={100}
                            duration={3}
                            deslay={3}/></p>
                        </div>
                        <p className="home__section6-desc">{data.strapiHomepage.counter4_desc}</p>
                    </div>
                </div>
            </div>

            <div className="home__section7-wrapper">
                <h2 className="home__section7-title">{data.strapiHomepage.grywalization_title}</h2>
                <img src={`${strapiHost}${data.strapiHomepage.grywalization_icon.url}`}/>
                <p className="home__section7-desc">{data.strapiHomepage.grywalization_desc}</p>
                
                <button
                className="home__section7-button"
                type="button"
                ><Link to="/">
                Dowiedz się więcej</Link>
                </button>
                
            </div>

        </div>

        <Footer/>
        </>
;
};

const query = graphql`
    query {
    strapiHomepage {
        Title
        title2
        title3
        title5
        description_clutch
        description
        description2
        description3
        description4
        description5
        description2_technology
        description3_technology
        description4_technology
        description_technology
        counters_title
        number1
        number2
        number3
        number4
        counter1_desc
        counter2_desc
        counter3_desc
        counter4_desc
        grywalization_title
        grywalization_desc
        grywalization_icon {
            url
        }
        technology1 {
            url
        }
        technology2 {
            url
        }
        technology3 {
            url
        }
        technology4 {
            url
        }
        logo {
            url
        }
        icon1 {
            url
        }
        icon2 { 
            url
        }
        icon3 {
            url
        }
        icon4 {
            url 
        }
        img {
            url
        }

    }
}
`;

export default IndexPage;
