import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/offer.scss";
import Footer from "../components/footer";

const offer = () => {

    // eslint-disable-next-line
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const single = data.allStrapiOffer.nodes[0].offer_single


    return <> 
    <Nav/>

    <div className="offer__wrapper">
        <h2 className="offer__title">{data.strapiOffer.title}</h2>
        <p className="offer__description">{data.strapiOffer.description}</p>
            {/* <div className="offer__single"> */}
            {single.map(({description, img, link, lista, title}) => 
            <div className="offer__single-offer">
                <div className="offer__single-wrapper">
                    <img src={`${strapiHost}${img.url}`} className="offer__img2"/>
                    <h2 className="offer__single-title">{title}</h2>
                </div>
                <p className="offer__single-desc1">{description}</p>
                <li className="offer__single-list">{lista}</li>
                <button
                className="offer__single-button"
                type="button"
                ><Link to={link}>
                Dowiedz się wiecej </Link>
                </button>
            </div>)}
        {/* </div> */}
    </div>
    <Footer/>
    </>

}

const query = graphql`
    query {
        strapiOffer {
            description
            title
        }

        allStrapiOffer {
            nodes {
                offer_single {
                description
                lista
                title
                link
                img {
                    url
                    }
                }
            }
        }
}`;

export default offer;