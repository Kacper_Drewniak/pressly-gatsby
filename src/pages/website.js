import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/websites.scss";
import Footer from "../components/footer";

const website = () => {
    // eslint-disable-next-line
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const web = data.allStrapiWebsite.nodes[0].website_single
    const web2 = data.allStrapiWebsite.nodes[0].realization
    return <>
    <Nav/>
    <div className="website__section1">
        <div className="website__section1-left">
            <h1 className="website__section1-title">{data.strapiWebsite.title}</h1>
            <p className="website__section1-desc">{data.strapiWebsite.description}</p>
        </div>
        <div className="website__section1-right">
        <img src={`${strapiHost}${data.strapiWebsite.img.url}`} className="websites__img"/>
        </div>
    </div>
    
    <div className="website__single-wrapper">
        <div className="website__single-content">
        {web.map(({title, number, description}) =><div className="website__single">
                    <h2 className="website__single-title">{title}</h2>
                    <p className="website__single-description">{description}</p>
                    <p className="website__single-number">{number}</p>
                </div>
                )}
                </div>
    </div>
    <div className="website__section2">
        <div className="website__section2-left">
        <img src={`${strapiHost}${data.strapiWebsite.img.url}`}/>
        </div>
        <div className="website__section2-right">
            <h2 className="website__section2-title">{data.strapiWebsite.title_section2}</h2>
            <p className="website__section2-description">{data.strapiWebsite.description_section2}</p>
        </div>

    </div>
    <div className="website__realizations">
        <h2 className="website__realizations-title">{data.strapiWebsite.title_section3}</h2>
        <div className="website__realizations-content">
        {web2.map(({title, img, description_back}) => <div className="realizations__single">
        <img src={`${strapiHost}${img.url}`} className="realizations__img2"/>
        <div className="realizations__single-back">
            <p>{description_back}</p>
        </div>
        <h2 className="realizations__des">{title}</h2>
            </div>)}
        </div>
    </div>
    <button
                className="website__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>
    <Footer/>
    </>
};
const query = graphql`
    query {
        strapiWebsite {
            title
            title2
            title_section3
            title_section2
            description
            img_section2 {
                url
            }
            description_section2
            img {
                url
            }
            img_section2 {
                url
            }
            }
            allStrapiWebsite {
            nodes {
                website_single {
                description
                number
                title
                }
                realization {
                    title
                    img {
                        url
                    }
                    description_back
                }
            }
        }
    }`;

export default website;